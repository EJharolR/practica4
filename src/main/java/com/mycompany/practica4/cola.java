package com.mycompany.practica4;
/**
 *
 * @author Jharol Rodriguez
 */
public class cola {
    private pila P1,P2;
    public cola(){
        P1=new pila();
        P2=new pila();
    }
    public void IntroducirDato(int data){
        while(!P1.isEmpty()){
            P2.push(P1.pop());
        }
        P1.push(data);
        while(!P2.isEmpty()){
            P1.push(P2.pop());
        }
    }
    public boolean Vacio(){
        return (P1.isEmpty());
    }
    public int QuitarDato(){
        int temp=-1;
        if(this.Vacio())
            System.out.println("La Cola Esta Vacia");
        else
            temp=P1.pop();
        return temp;
    }
    public int Primero(){
        int temp=-1;
        if(this.Vacio())
            System.out.println("La Cola Esta Vacia");
        else
            temp=P2.pop();
        return temp;
    }
}
