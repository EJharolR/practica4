package com.mycompany.practica4;
/**
 *
 * @author Jharol Rodriguez
 */
public class pila {
    private int arr[];
    private int tope;
    public pila(){
        this.arr=new int[5];
        this.tope=-1;
    }
    public boolean isEmpty(){
        return(this.tope==-1);
    }
    public boolean isFull(){
        return(this.arr.length-1==this.tope);
    }
    public void push(int element){
        if(this.isFull()){
            int x[]=new int[this.arr.length+2];
            for(int i=0;i<this.arr.length;i++)
                x[i]=this.arr[i];
            this.arr=x;
        }
        this.arr[++tope]=element;
    }
    public int peek(){
        return arr[tope];
    }
    public int pop(){
        int temp=-1;
        if(!this.isEmpty()){
            temp=arr[tope];
            arr[tope--]=-1;
        }
        return temp;
    }
}
